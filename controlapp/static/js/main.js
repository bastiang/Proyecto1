function buscar(){
	keyUsuario = $('#keyUsuario').val()
	if (keyUsuario != '') {
		$.ajax({
			type:'POST',
        	url:'/estadomaquina/',
        	dataType:'json',
        	data:{'csrfmiddlewaretoken':csrftoken,'keyUsuario':keyUsuario},
        	success:function(mydata){
        		$('#keyUsuario').val('')
        		$('#modeloMaquina').html(mydata[0].modeloMaquina)
        		$('#busqueda').html(
        		"<div class='col'>Key Actual:<input type='text' id='keyBusqueda' class='form-control' readonly>"+
				"<input type='text' id='comandoSocket' class='form-control' placeholder='codigo de busqueda'><br>"+
				"<input type='button' class='btn btn-dark' value='Buscar' onclick='buscarSocket();'>"+
				"<input type='button' class='btn btn-dark' value='Limpiar' onclick='limpiar();'></div>"
        		)
        		$('#keyBusqueda').val(keyUsuario)
        		$('#mensajeSocket').html('')
        	}
		})
	}
	else {

	}
}

function buscarSocket(){
	comandoSocket = $('#comandoSocket').val()
	keyBusqueda = $('#keyBusqueda').val()
	if (comandoSocket != '') {
		$.ajax({
			type:'POST',
        	url:'/buscarsocket/',
        	dataType:'json',
        	data:{'csrfmiddlewaretoken':csrftoken,'comandoSocket':comandoSocket,'keyBusqueda':keyBusqueda},
        	success:function(mydata){
        		msjSocket = mydata[0].mensajeSocket
        		if (msjSocket =='error') {
        			alert('ERROR DE CONEXION')
        		}
        		else{
        			$('#mensajeSocket').prepend('<tr><th>'+msjSocket+'</th></tr>').css('color','black')
        		}
        	}
		})
	}
	else {
		alert('INGRESE UNA KEY')
	}
}

function limpiar(){
	$('#mensajeSocket').html('')
}