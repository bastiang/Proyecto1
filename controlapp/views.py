from django.shortcuts import render
import socket 
from django.http import HttpResponseRedirect,HttpResponse
from controlapp import models
import json


def index(request):
	return render(request ,'base.html')
	
def estado(request):
	if request.method == 'POST':
		if request.is_ajax():
			keyMaquina = request.POST['keyUsuario']
			key = models.Maquina.objects.get(key_maquina=keyMaquina)
			modeloMaquina = str(key.modelo_maquina)

	mydata=[{'modeloMaquina':modeloMaquina}]
	return HttpResponse(json.dumps(mydata))

def buscarsocket(request):
	msg=''
	if request.method == 'POST':
		if request.is_ajax():
			keyMaquina = request.POST['keyBusqueda']
			key = models.Maquina.objects.get(key_maquina=keyMaquina)
			ip_maquina = str(key.ip_maquina)
			SERVER = ip_maquina
			PORT = 7000
			try:
				client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
				client.connect((SERVER, PORT))
				comando = request.POST['comandoSocket']
				client.sendall(bytes(str(comando),'UTF-8'))
				msg = client.recv(1024)
				print(msg.decode())
				mensajeSocket=msg.decode()
			except Exception as e:
				mensajeSocket='error'
	mydata=[{'mensajeSocket':mensajeSocket}]
	return HttpResponse(json.dumps(mydata))
